package game_board

class Board(var grid: List<List<String>> = listOf(
    listOf(" ", " ", " "),
    listOf(" ", " ", " "),
    listOf(" ", " ", " ")
))

fun Board.print() {
    printLine()
    this.grid.forEach { row ->
        println(row.joinToString(separator = " |"))
        printLine()
    }
}

private fun printLine() = println("_________")

fun main() {
    val board = Board()
    board.print()
}