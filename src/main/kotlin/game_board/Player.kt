package game_board

class Player(
    var winner: Boolean = false,
    val moveSymbol: String = listOf("O", "X").shuffled().first(),
    val name: String = "$moveSymbol player"
) {


    fun move(boardState: Board) {

        println("${this.name} please make your move: ")
        // Change board and return it
    }
}