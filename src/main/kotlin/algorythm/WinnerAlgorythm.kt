package algorythm

import game_board.Board

fun isWinner(board: Board): Boolean {
    return when {
        board.grid.any { it.all { symbol -> isX(symbol) } } ||
                board.grid.any { it.all { symbol -> isO(symbol) } } -> true

        board.grid.all { isX(it[0]) } ||
                board.grid.all { isX(it[1]) } ||
                board.grid.all { isX(it[2]) } -> true

        board.grid.all { isO(it[0]) } ||
                board.grid.all { isO(it[1]) } ||
                board.grid.all { isO(it[2]) } -> true

        checkDiagonally(board.grid) -> true
        else -> false
    }
}

private fun isX(symbol: String) = symbol == "X"

private fun isO(symbol: String) = symbol == "O"

private fun checkDiagonally(list: List<List<String>>): Boolean {
    return when {
        list.all { isO(it[list.indexOf(it)]) } ||
                list.all { isX(it[list.indexOf(it)]) } ||
                list.all { isO(it[list.indexOf(it) - list.size]) } ||
                list.all { isX(it[list.indexOf(it) - list.size]) } -> true
        else -> false
    }
}
